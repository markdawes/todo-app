package com.rave.todoapp.model

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface TaskDao {

    @Query("Select * from Tasks")
    fun getTasks(): LiveData<List<Task>>

    @Insert(onConflict = OnConflictStrategy.IGNORE, entity = Task::class)
    fun insertTask(vararg task: Task)

    @Delete
    fun removeTask(vararg task: Task)
}
