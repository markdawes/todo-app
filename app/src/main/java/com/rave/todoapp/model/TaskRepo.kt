package com.rave.todoapp.model

import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Repository for holding tasks.
 *
 * @property dao
 * @constructor Create empty Task repo
 */
class TaskRepo(private val dao: TaskDao) {

    val readAllTasks: LiveData<List<Task>> = dao.getTasks()

    /**
     * Add tasks ton database.
     *
     * @param tasks
     */
    suspend fun insertTasks(vararg tasks: Task) = withContext(Dispatchers.IO) {
        dao.insertTask(*tasks)
    }

    /**
     * Remove tasks from database.
     *
     * @param tasks
     */
    suspend fun deleteTasks(vararg tasks: Task) = withContext(Dispatchers.IO) {
        dao.removeTask(*tasks)
    }
}
