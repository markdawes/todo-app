package com.rave.todoapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Class for task data.
 *
 * @property id
 * @property name
 * @property description
 * @property isCompleted
 * @constructor Create empty Task
 */
@Entity(tableName = "Tasks")
data class Task(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "Name") val name: String,
    @ColumnInfo(name = "Description") val description: String,
    @ColumnInfo(name = "Completed") val isCompleted: Boolean
)
