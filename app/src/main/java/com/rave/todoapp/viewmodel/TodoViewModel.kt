package com.rave.todoapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.rave.todoapp.model.Task
import com.rave.todoapp.model.TaskDatabase
import com.rave.todoapp.model.TaskRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Viewmodel serving as layer between repo and application.
 *
 * @constructor
 *
 * @param application
 */
class TodoViewModel(application: Application) : AndroidViewModel(application) {

    private val repo: TaskRepo
    var tasks: LiveData<List<Task>>

//    private val _tasks: MutableStateFlow<List<Task>> = MutableStateFlow(emptyList())
//    val tasks: StateFlow<List<Task>> get() = _tasks

    init {
        val taskDao = TaskDatabase.getDatabase(application).getTaskDao()
        repo = TaskRepo(taskDao)
        tasks = repo.readAllTasks
    }

    /**
     * Add task to repo.
     *
     * @param task
     */
    fun addTask(task: Task) = viewModelScope.launch(Dispatchers.IO) {
        repo.insertTasks(task)
    }
}
