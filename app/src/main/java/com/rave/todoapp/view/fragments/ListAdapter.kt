package com.rave.todoapp.view.fragments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.todoapp.R
import com.rave.todoapp.model.Task
import kotlinx.android.synthetic.main.custom_row.view.id_txt
import kotlinx.android.synthetic.main.custom_row.view.taskName_txt

/**
 * Adapter for recyclerview.
 *
 * @constructor Create empty List adapter
 */
class ListAdapter : RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    private var taskList = emptyList<Task>()

    /**
     * My viewholder class.
     *
     * @constructor
     *
     * @param itemView
     */
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_row, parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = taskList[position]
        holder.itemView.id_txt.text = "*"
        holder.itemView.taskName_txt.text = currentItem.name
    }

    override fun getItemCount(): Int {
        return taskList.size
    }

    /**
     * Set data.
     *
     * @param task
     */
    fun setData(task: List<Task>) {
        this.taskList = task
        notifyDataSetChanged()
    }
}
