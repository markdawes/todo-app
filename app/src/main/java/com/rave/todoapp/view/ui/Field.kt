package com.rave.todoapp.view.ui

import android.util.Patterns
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

/**
 * Field class to help with creating forms.
 *
 * @property name
 * @property label
 * @property validators
 * @property textVal
 * @constructor Create empty Field
 */
class Field(
    val name: String,
    val label: String = "",
    val validators: List<Validator>,
    val textVal: String = ""
) {
    var text: String by mutableStateOf(textVal)
    var lbl: String by mutableStateOf(label)
    var hasError: Boolean by mutableStateOf(false)

    /**
     * Clear.
     *
     */
    fun clear() {
        text = ""
    }

    /**
     * Show error.
     *
     * @param error
     */
    private fun showError(error: String) {
        hasError = true
        lbl = error
    }

    /**
     * Hide error.
     *
     */
    private fun hideError() {
        lbl = label
        hasError = false
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun Content() {
        TextField(
            value = text,
            isError = hasError,
            label = { Text(text = lbl) },
            modifier = Modifier.padding(10.dp),
            onValueChange = { value ->
                hideError()
                text = value
            }
        )
    }

    /**
     * Validate.
     *
     * @return
     */
    fun validate(): Boolean {
        return validators.map {
            when (it) {
                is Email -> {
                    if (!Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
                        showError(it.message)
                        return@map false
                    }
                    true
                }
                is Required -> {
                    if (text.isEmpty()) {
                        showError(it.message)
                        return@map false
                    }
                    true
                }
                is Regex -> {
                    if (!it.regex.toRegex().containsMatchIn(text)) {
                        showError(it.message)
                        return@map false
                    }
                    true
                }
            }
        }.all { it }
    }
}

/**
 * For validating fields passed into form.
 *
 * @constructor Create empty Form state
 */
class FormState {
    var fields: List<Field> = listOf()
        set(value) {
            field = value
        }

    /**
     * Validation.
     *
     * @return
     */
    fun validate(): Boolean {
        var valid = true
        for (field in fields) if (!field.validate()) {
            valid = false
            break
        }
        return valid
    }

    /**
     * Get data.
     *
     * @return
     */
    fun getData(): Map<String, String> = fields.map { it.name to it.text }.toMap()
}

@Composable
fun Form(state: FormState, fields: List<Field>) {
    state.fields = fields

    Column {
        fields.forEach {
            it.Content()
        }
    }
}

private const val EMAIL_MESSAGE = "invalid email address"
private const val REQUIRED_MESSAGE = "this field is required"
private const val REGEX_MESSAGE = "value does not match the regex"

/**
 * Validator.
 *
 * @constructor Create empty Validator
 */
sealed interface Validator

/**
 * Email.
 *
 * @property message
 * @constructor Create empty Email
 */
open class Email(var message: String = EMAIL_MESSAGE) : Validator

/**
 * Required.
 *
 * @property message
 * @constructor Create empty Required
 */
open class Required(var message: String = REQUIRED_MESSAGE) : Validator

/**
 * Regex.
 *
 * @property message
 * @property regex
 * @constructor Create empty Regex
 */
open class Regex(var message: String, var regex: String = REGEX_MESSAGE) : Validator
