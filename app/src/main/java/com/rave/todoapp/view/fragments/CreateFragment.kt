package com.rave.todoapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.rave.todoapp.R
import com.rave.todoapp.model.Task
import com.rave.todoapp.view.ui.AppBar
import com.rave.todoapp.view.ui.Field
import com.rave.todoapp.view.ui.Form
import com.rave.todoapp.view.ui.FormState
import com.rave.todoapp.view.ui.Required
import com.rave.todoapp.view.ui.theme.TodoAppTheme
import com.rave.todoapp.viewmodel.TodoViewModel
import kotlinx.coroutines.runBlocking
import kotlin.random.Random

/**
 * Screen for creating a task.
 *
 * @constructor Create empty Create fragment
 */
class CreateFragment : Fragment() {

    @Suppress("LateinitUsage")
    private lateinit var viewModel: TodoViewModel

    @OptIn(ExperimentalMaterial3Api::class)
    @Suppress("LongMethod")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this)[TodoViewModel::class.java]
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                TodoAppTheme {
                    Scaffold(topBar = {
                        AppBar(
                            title = "Create Task",
                            icon = Icons.Default.ArrowBack
                        ) {
                            findNavController().navigateUp()
                        }
                    }) {
                        Surface(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(it),
                            color = Color.LightGray
                        ) {
                            val state by remember { mutableStateOf(FormState()) }
                            Column {
                                Form(
                                    state = state,
                                    fields = listOf(
                                        Field(
                                            name = "name",
                                            label = "Task Name",
                                            validators = listOf(Required())
                                        ),
                                        Field(
                                            name = "description",
                                            label = "Task Description",
                                            validators = listOf(Required())
                                        )
                                    )
                                )
                                Button(onClick = {
                                    if (state.validate()) {
                                        @Suppress("MagicNumber")
                                        runBlocking {
                                            viewModel.addTask(
                                                Task(
                                                    id = Random.nextInt(0, 500_000_000),
                                                    name = state.fields[0].text,
                                                    description = state.fields[1].text,
                                                    isCompleted = false
                                                )
                                            )
                                        }
                                        findNavController().navigate(R.id.action_createFragment_to_listFragment)
                                    }
                                }) {
                                    Text("Submit")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
