package com.rave.todoapp.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.rave.todoapp.R
import com.rave.todoapp.viewmodel.TodoViewModel

/**
 * Entry point for application.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : FragmentActivity() {
    @Suppress("LateinitUsage")
    private lateinit var navController: NavController

    @Suppress("UnusedPrivateMember")
    private val todoViewModel by viewModels<TodoViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
    }
}
