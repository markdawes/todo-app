package com.rave.todoapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.todoapp.R
import com.rave.todoapp.viewmodel.TodoViewModel
import kotlinx.android.synthetic.main.fragment_list.view.floatingActionButton
import kotlinx.android.synthetic.main.fragment_list.view.recyclerview

/**
 * Screen for listing tasks.
 *
 * @constructor Create empty List fragment
 */
class ListFragment : Fragment() {

    @Suppress("LateinitUsage")
    private lateinit var viewModel: TodoViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        val adapter = ListAdapter()
        val recyclerView = view.recyclerview

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        viewModel = ViewModelProvider(this)[TodoViewModel::class.java]
        viewModel.tasks.observe(
            viewLifecycleOwner,
            Observer { task ->
                adapter.setData(task)
            }
        )

        view.floatingActionButton.setOnClickListener {
            findNavController().navigate(R.id.action_listFragment_to_createFragment)
        }

        return view
    }
}
